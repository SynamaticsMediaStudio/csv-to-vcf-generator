# VCard Express

This is a simple web application that converts CSV files to VCF files. It can be used to convert contact lists to vCard files which can be imported into various devices and applications.

## Features

- Supports CSV files with headers or without
- Supports CSV files with different delimiters (comma, tab, etc.)
- Supports CSV files with different encodings (UTF-8, UTF-16, etc.)
- Converts CSV data to vCard format
- Downloads the generated vCard file

## Installation

1. Clone the repository
2. Run `composer install` to install the dependencies

## Usage

1. Upload a CSV file using the file input field
2. Select the column names for the contact details (name and phone number)
3. Click the "Generate" button to download the vCard file

## Contributing

Contributions are welcome. Please submit a pull request with your changes.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE.md) file for details.
