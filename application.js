document.addEventListener("DOMContentLoaded",init);


function init(){
    let button = document.getElementById("genbutton");
    button.addEventListener('click',(event)=>{
        event.preventDefault();
        let data = document.getElementById("data");
        let phone = document.getElementById("phone");
        let name = document.getElementById("name");
        let disable_first = document.getElementById("disable_first");
        if(data){
            let text   = "";
            let items = JSON.parse(data.value);
            for (let index = 0; index < items.length; index++) {
               if(index == 0){
                   if(disable_first.checked){
                    continue;
                   }
               }

                const element = items[index];
                text    += "BEGIN:VCARD\nVERSION:4.0\n";
                text    += "FN:"+element[name.value]+"\n";
                text    += "EMAIL:\n";
                text    += "TEL;type=CELL:"+element[phone.value]+"\n";
                text    += "END:VCARD\n\n";            
            }
            text    = unescape( encodeURIComponent(text));
            download('export.vcf', text);
        }
    })
    function download(filename, text) {
        var element = document.createElement('a');
        element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
        element.setAttribute('download', filename);
    
        element.style.display = 'none';
        document.body.appendChild(element);
    
        element.click();
    
        document.body.removeChild(element);
    }
}