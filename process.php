<?php 
require 'vendor/autoload.php';

use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\IOFactory;

if(empty($_FILES)):
    die("No File");
endif;

$file       = $_FILES['file'];
$fileItem   = IOFactory::identify($file["tmp_name"]);
$reader     = IOFactory::createReader($fileItem);
if($fileItem == "Csv"){
    $reader->setDelimiter(",")->setReadDataOnly(true);
}
$spreadsheet = $reader->load($file["tmp_name"]);
$worksheet   = $spreadsheet->getActiveSheet();
$rows = $worksheet->toArray();
$header = $rows[0];
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.0/css/bootstrap.min.css">
    <script src="./application.js"></script>
    <title>Document</title>
</head>

<body>
    <div class="container">

        <div class="row">
            <div class="col-sm-12 col-md-6">
            <div class="row">
            
                <div class="form-group col-sm-12 col-md-6">
                    <label for="name">Name Column</label>
                    <select name="name" id="name" class="form-control">
                        <?php for ($i=0; $i < count($header) ; $i++):?>
                        <option value="<?php echo $i;?>"><?php echo $header[$i];?></option>
                        <?php endfor;?>
                    </select>
                </div>
                <div class="form-group col-sm-12 col-md-6">
                    <label for="phone">Phone Number Column</label>
                    <select name="phone" id="phone" class="form-control">
                        <?php for ($i=0; $i < count($header) ; $i++):?>
                        <option value="<?php echo $i;?>"><?php echo $header[$i];?></option>
                        <?php endfor;?>
                    </select>
                </div>
                <div class="form-group col-sm-12 col-md-6">
                    <textarea name="data" id="data" class="form-control" cols="30" hidden
                        rows="10"><?php echo json_encode($rows);?></textarea>
                </div>
                <div class="form-group col-sm-12 col-md-12">
                    <input type="checkbox" name="disable_first" id="disable_first" value="true">
                    <label for="disable_first">Disable First Row?</label>
                </div>
                <div class="form-group col-sm-12">
                    <button id="genbutton" class="btn btn-primary">Generate</button>
                </div>
            </div>
            </div>
        </div>
    </div>
</body>

</html>